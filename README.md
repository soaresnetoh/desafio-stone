# Desafio DevOps Stone  


O objetivo desse desafio é termos um cluster Kubernetes e dentro dele ter uma ferramenta onde conseguimos medir a latência entre os nodes do cluster.  

#### Etapas  
- Criar um cluster de EKS em pelo menos 2 Availability Zones  
- Configurar uma instância de Prometheus rodando no EKS  
- Encontrar uma solução de monitoramento de rede do Kubernetes. Só queremos saber se todos os nodes estão conseguindo conversar entre si e qual é a latência  
- Configurar a solução de monitoramento de rede no EKS  
- Coletar as métricas usando o Prometheus  

#### Entregas  
- Código feito todo em terraform em um repositório git  
- Documentação de uso  
- Apresentar a solução  

#### Considerações  
- Principal, entenda tudo que esta fazendo e utilizando  
- Pode utilizar modulos prontos do Terraform  
- Pode usar alguma ferramenta pronta para medir a latência  
- Pense em uma solução simples ([Principio KISS](https://pt.wikipedia.org/wiki/Princ%C3%ADpio_KISS))  
- O ambiente de testes provavelmente vai usar recursos fora da "free-tier" da AWS. Você pode entrar em contato com o suporte deles, após a apresentação, para estorno dos gastos explicando que foi para um processo de seleção  
- Não esqueça de destruir o ambiente após seus testes para evitar custos desnecessários.  

---
###Projeto

#### Author: Hernani Soares


O objetivo desse desafio é termos um cluster Kubernetes e dentro dele ter uma ferramenta onde conseguimos medir a latência entre os nodes do cluster.

#### Etapas
* [x] Criar um cluster de EKS em pelo menos 2 Availability Zones
* [x] Configurar uma instância de Prometheus rodando no EKS
* [x] Encontrar uma solução de monitoramento de rede do Kubernetes. Só queremos saber se todos os nodes estão conseguindo conversar entre si e qual é a latência
* [x] Configurar a solução de monitoramento de rede no EKS
* [x] Coletar as métricas usando o Prometheus


# Paso a passo para concepção do projeto

1- **User AWS**  
Criação de um usuario na AWS com as permissões necessária para criação do projeto e seus recursos. 

Baixe as credenciais

2- **Instalar ferramentas**  
- Instalar as ferramentas:

[x] wget / unzip / curl
```
# sudo apt install wget unzip curl -y
```

[x] Terraform

```
# wget https://releases.hashicorp.com/terraform/1.0.1/terraform_1.0.1_linux_amd64.zip && \
unzip terraform_1.0.1_linux_amd64.zip && \
sudo mv terraform /usr/local/bin/ && \
rm -Rf terraform_1.0.1_linux_amd64.zip && \
rm -Rf terraform
```
[x] kubectl
```
# curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && \
chmod +x kubectl && \
sudo mv kubectl /usr/local/bin/ && \
rm -Rf kubectl
```
[x] awscli (na versão 2 não pe necessárias mais a instalação previa do python)
```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
unzip awscliv2.zip && \
sudo ./aws/install && \
aws --version
```
3- **Credencias Aws x Terraform**  
As credenciais aws usadar pelo terraform para provisionar a estrutura, foram armazenadas no arquivo de configurações local *_(~/.aws/credentials)_* e referenciadas no arquivo providers.tf. 

```
# aws configure
AWS Access Key ID [none]: <ACCESS_KEY>
AWS Secret Access Key ID [none]: <SECRET_KEY>
Default region name [none]: <REGION>
Default output format [none]: <OUTPUT>
```

Tem a opção de configurar um profile diferente de default --- lembre que sempre deverá usar _*--profile <PROFILE>*_ no final dos comandos

```
# aws configure --profile <PROFILE>
AWS Access Key ID [none]: <ACCESS_KEY>
AWS Secret Access Key ID [none]: <SECRET_KEY>
Default region name [none]: <REGION>
Default output format [none]: <OUTPUT>
```

4- **Criar um Bucket no S3 para armazenar o tfstate do Terraform**  
Para isso foi necessário criar um arquivo *_(s3/bucket-s3.tf)_* para criação do bucket separadamente. Este passo poderia ser feito na console, porem o projeto é totalmente IaC. Lembrando que o nome do bucket deve ser **ÚNICO** em toda plataforma AWS.

```
# cd ./s3
# terraform init
# terraform plan
# terraform apply
```

5- **Subir toda estrutura necessária para que o cluster possa existir.**  
Use o terraform para provisionar a estrutura do cluster  *_(eks/*.tf)_*. Foram usados modulos para criação do ambiente, abstraindo a criação de varios recursos como vpc, subnets, routers.  

```
# cd ./eks
# terraform init
# terraform plan
# terraform apply
```

6- **Para este projeto foram usados**  
 ##### Providers
  | Provider | Versão |
  | --- | --- |
  | aws | >= 3.47.0 |
  | terraform | > 0.14 |
  | kubernetes | >= 2.3.2 |
  | hashicorp/random | 3.1.0 |
  | hashicorp/local | 2.1.0 |
  | hashicorp/null | 3.1.0 |
  | hashicorp/template | 2.2.0 |
  

 ##### Alguns recursos

 - Aws S3
 - Aws EKS
 - Aws VPC
 - Aws EC2
 - terraform
 - kubectl
 - wget
 - aws cli

# Stack de Observabilidade

após subir o cluster, subir a stack de observabilidade da pasta _*./monitoring/*_ 

(precisa colocar este passos no kubernetes)  

```
*** a completar ***
```


---
## Alguns links usados como referencia

### Configuração do backend remoto:  
https://learn.hashicorp.com/tutorials/terraform/aws-remote?in=terraform/aws-get-started

Para usar o backend remoto, primeiro criei um bucker para depois rodar o terraform do projeto.   

### Configuração eks aws  

https://learn.hashicorp.com/tutorials/terraform/eks


Este ultimo usei como referencia para criar o cluster porem os modulos foram pesquisados :  
https://registry.terraform.io/browse/modules  

### Documentação do Kubernetes para aplicar manifesto Kubernetes provider  
https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/guides/getting-started


### Configurar kubeconfig  

```
# aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name) --profile terraform
```

Este comando o arquivo de configuração para acessar o cluster atraves do kubectl

_*~/.kube/config*_
