provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "terraform"
}

resource "aws_s3_bucket" "bucket-backend" {
  bucket = "terraform-deploy-backend-desafio"
  versioning {
    enabled = true
  }
  lifecycle {
    prevent_destroy = true
  }
  tags = {
    Name    = "backend-S3"
    Project = "desafio-stone-devops"

  }
}