variable "region" {
  default     = "us-east-1"
  description = "AWS region Norte da Virginia"
}
variable "tag_projeto" {
  default = "desafio-stone-devops"
}

locals {
  cluster_name = "desafio_stone-eks-${random_string.suffix.result}"
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}
